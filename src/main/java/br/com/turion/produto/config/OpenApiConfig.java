package br.com.turion.produto.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
public class OpenApiConfig {

    @Value("${produto.server}")
    private String server;

    @Bean
    public OpenAPI customOpenAPI() {

        return new OpenAPI()
                .info(metaData())
                .addServersItem(new Server().url(server))
                .addServersItem(new Server().url("http://localhost:42099"))
                .components(new Components());
    }


    private Info metaData() {
        return new Info()
                .title("API - Manutenção de Produtos")
                .version("1.0")
                .description("Manutenção de Produtos");
    }
}
