package br.com.turion.produto.controller;

import br.com.turion.produto.model.data.ProdutoData;
import br.com.turion.produto.service.ProdutoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(value = "/produto", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Produtos", description = "Manutenção de produtos")
public class ProdutoController {

    private final ProdutoService produtoService;

    @Autowired
    public ProdutoController(ProdutoService produtoService) {
        this.produtoService = produtoService;
    }

    @Operation(summary = "Retorna os dados de todos os produtos")
    @GetMapping
    public ResponseEntity<List<ProdutoData>> findAll() {
        List<ProdutoData> produtoDataList = produtoService.findAll();

        if (produtoDataList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(produtoDataList);
    }

    @Operation(summary = "Retorna as informações de um produto com base no seu ID.")
    @GetMapping("/{id}")
    public ResponseEntity<ProdutoData> findId(@PathVariable Long id) {
        Optional<ProdutoData> produtoDataOptional = produtoService.findId(id);

        return produtoDataOptional.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
    }

    @Operation(summary = "Incluir um produto")
    @PostMapping
    public ResponseEntity<ProdutoData> insert(@Valid @RequestBody ProdutoData produto, HttpServletResponse response) {
        ProdutoData produtoData = produtoService.insert(produto).orElse(new ProdutoData());

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(produtoData.getId()).toUri();
        response.setHeader("Location", uri.toASCIIString());

        return ResponseEntity.status(HttpStatus.CREATED).body(produtoData);
    }

    @Operation(summary = "Alterar um produto já existente utilizando como referencia o seu ID")
    @PutMapping
    public ResponseEntity<ProdutoData> update(@Valid @RequestBody ProdutoData produto, HttpServletResponse response) {
        ProdutoData produtoData = produtoService.update(produto).orElse(new ProdutoData());

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(produtoData.getId()).toUri();
        response.setHeader("Location", uri.toASCIIString());

        return ResponseEntity.ok(produtoData);
    }

    @Operation(summary = "Excluir um produto já existente informando o ID.")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id, HttpServletResponse response) {
        produtoService.delete(id);
        response.setStatus(HttpStatus.OK.value());
    }

    @Operation(summary = "Retorna a lista dos tipos de produtos")
    @ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(example =
            "[\n" +
            "  {\n" +
            "    \"codigo\": \"NULO\",\n" +
            "    \"descricao\": \"Não definido\"\n" +
            "  }\n" +
            "]")))
    @GetMapping("/tipo")
    public ResponseEntity<List<Map<String, String>>> findTipoProduto() {
        List<Map<String, String>> selectList = produtoService.findTipoProduto();
        return ResponseEntity.ok(selectList);
    }

}
