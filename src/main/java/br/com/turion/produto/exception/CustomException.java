package br.com.turion.produto.exception;

public class CustomException extends RuntimeException {

    private final String field;

    public CustomException(String message) {
        super(message);
        this.field = "";
    }

    public CustomException(String message, String field) {
        super(message);
        this.field = field;
    }

    public String getField() {
        return this.field;
    }

}
