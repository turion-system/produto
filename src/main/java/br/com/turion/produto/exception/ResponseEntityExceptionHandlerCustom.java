package br.com.turion.produto.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ControllerAdvice
public class ResponseEntityExceptionHandlerCustom extends ResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    public ResponseEntityExceptionHandlerCustom(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String userMessage = messageSource.getMessage("mensagem.invalida", null, LocaleContextHolder.getLocale());
        String developerMessage = ex.getCause() == null ? ex.toString() : ex.getCause().toString();
        List<ErrorMessage> errorMessageList = Collections.singletonList(new ErrorMessage(userMessage, developerMessage, "", ex.getMessage()));

        return handleExceptionInternal(ex, errorMessageList, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        List<ErrorMessage> errorMessageList = new ArrayList<>();

        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            String userMessage = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
            String developerMessage = fieldError.toString();
            errorMessageList.add(new ErrorMessage(userMessage, developerMessage, fieldError.getField(), fieldError.getCode()));
        }

        return handleExceptionInternal(ex, errorMessageList, headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({CustomException.class})
    public ResponseEntity<Object> handlerEmptyResultDataAccessException(CustomException ex, WebRequest request) {
        String userMessage = messageSource.getMessage(ex.getMessage(), null, LocaleContextHolder.getLocale());
        String developerMessage = ex.toString();
        List<ErrorMessage> errorMessageList = Collections.singletonList(new ErrorMessage(userMessage, developerMessage, ex.getField(), ex.getMessage()));

        return handleExceptionInternal(ex, errorMessageList, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Getter
    @Setter
    public static class ErrorMessage {

        private String userMessage;
        private String developerMessage;
        private String field;
        private String errorCode;

        public ErrorMessage() {
        }

        public ErrorMessage(String userMessage, String developerMessage, String field, String errorCode) {
            this.userMessage = userMessage;
            this.developerMessage = developerMessage;
            this.field = field;
            this.errorCode = errorCode;
        }
    }
}
