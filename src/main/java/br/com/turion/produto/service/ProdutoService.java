package br.com.turion.produto.service;

import br.com.turion.produto.exception.CustomException;
import br.com.turion.produto.model.Produto;
import br.com.turion.produto.model.TipoProduto;
import br.com.turion.produto.model.data.ProdutoData;
import br.com.turion.produto.repository.ProdutoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProdutoService {

    private final ProdutoRepository produtoRepository;
    private final ModelMapper modelMapper;

    public ProdutoService(ProdutoRepository produtoRepository, ModelMapper modelMapper) {
        this.produtoRepository = produtoRepository;
        this.modelMapper = modelMapper;
   }

    public List<ProdutoData> findAll() {
        return produtoRepository.findAll()
                .stream()
                .map(e -> modelMapper.map(e, ProdutoData.class))
                .collect(Collectors.toList());
    }

    public Optional<ProdutoData> findId(Long id) {
        return produtoRepository.findById(id).map(e -> modelMapper.map(e, ProdutoData.class));
    }

    @Transactional
    public Optional<ProdutoData> insert(ProdutoData produtoData) {

        Optional<Produto> produtoOptional = produtoRepository.findBySku(produtoData.getSku());
        if (produtoOptional.isPresent()) {
            throw new CustomException("produto.skuDuplicado", Produto.Fields.sku);
        }

        produtoOptional = produtoRepository.findByDescricaoIgnoreCase(produtoData.getDescricao());
        if (produtoOptional.isPresent()) {
            throw new CustomException("produto.descricaoDuplicada", Produto.Fields.descricao);
        }

        Produto produto = modelMapper.map(produtoData, Produto.class);
        produto.setId(null);

        produto = produtoRepository.saveAndFlush(produto);

        return this.findId(produto.getId());
    }

    @Transactional
    public Optional<ProdutoData> update(ProdutoData produtoData) {

        Optional<Produto> produtoOptional = produtoRepository.findById(produtoData.getId());
        if (produtoOptional.isEmpty()) {
            throw new CustomException("produto.idNaoEncontrado", Produto.Fields.id);
        }

        produtoOptional = produtoRepository.findBySku(produtoData.getSku());
        if (produtoOptional.isPresent() && !produtoOptional.get().getId().equals(produtoData.getId())) {
            throw new CustomException("produto.skuDuplicado", Produto.Fields.sku);
        }

        produtoOptional = produtoRepository.findByDescricaoIgnoreCase(produtoData.getDescricao());
        if (produtoOptional.isPresent() && !produtoOptional.get().getId().equals(produtoData.getId())) {
            throw new CustomException("produto.descricaoDuplicada", Produto.Fields.descricao);
        }

        Produto produto = modelMapper.map(produtoData, Produto.class);

        produto = produtoRepository.saveAndFlush(produto);

        return this.findId(produto.getId());
    }

    @Transactional
    public void delete(Long id) {
        Optional<Produto> optionalT = produtoRepository.findById(id);

        if (optionalT.isEmpty()) {
            throw new CustomException("produto.idNaoEncontrado", Produto.Fields.id);
        } else {
            produtoRepository.deleteById(id);
        }
    }

    public List<Map<String, String>> findTipoProduto() {
        List<Map<String, String>> list = new ArrayList<>();
        for (TipoProduto tipo : TipoProduto.values()) {
            Map<String, String> map = new HashMap<>();
            map.put("codigo", tipo.name());
            map.put("descricao", tipo.getDescricao());

            list.add(map);
        }
        return list;
    }
}
