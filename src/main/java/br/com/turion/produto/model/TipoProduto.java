package br.com.turion.produto.model;

public enum TipoProduto {

    NULO("Não definido"),
    PROPRIO("Produção própria"),
    TERCEIRO("Consignado de terceiros"),
    COMPRADO("Comprado"),
    ALUGADO("Alugado ou emprestado");

    private final String descricao;

    TipoProduto(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return this.descricao;
    }
}
