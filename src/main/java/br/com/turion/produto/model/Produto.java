package br.com.turion.produto.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "TAS_PRODUTO")
@Getter
@Setter
@ToString
@FieldNameConstants
public class Produto implements Serializable {

    private static final long serialVersionUID = -2859361422779110350L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 50)
    private String sku;
    @Column(length = 250)
    private String descricao;

    private LocalDate dataReferencia;
    private BigDecimal valorCusto;

    @Enumerated(EnumType.STRING)
    private TipoProduto tipo;

}
