package br.com.turion.produto.model.data;

import br.com.turion.produto.model.TipoProduto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@ToString
public class ProdutoData implements Serializable {

    private static final long serialVersionUID = 1669792467261347088L;

    @Schema(description = "Identificador único do produto, deverá ser informado apenas na alteração do produto.", example = "1")
    private Long id;

    @Schema(description = "Identificação do produto internamente para a empresa", example = "10.125.78.E", required = true)
    @Size(min = 5, max = 10)
    private String sku;
    @Schema(description = "Descrição do produto", example = "Garrafa vidro tipo A1", required = true)
    @Size(min = 5, max = 250)
    private String descricao;
    @Schema(description = "Data indicativa do inicio do calculo de custo", example = "2019-10-21", required = true)
    @Past
    private LocalDate dataReferencia;
    @DecimalMin("1.00")
    @Schema(description = "Valor de custo previsto", example = "13.17", required = true)
    private BigDecimal valorCusto;
    @Schema(description = "Tipo de aquisição do produto", example = "PROPRIO", required = true)
    private TipoProduto tipo;

    public ProdutoData() {
        this.id = 0L;
        this.sku = "";
        this.descricao = "";
        this.dataReferencia = LocalDate.now();
        this.valorCusto = BigDecimal.ZERO;
        this.tipo = null;
    }
}
